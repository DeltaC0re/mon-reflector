package net.ccmob.mon;

class MONConverter {

	static String convertClassValueToString(Class<?> destClass, Object value) {
		String stringValue = null;
		Class<?> destinationClass = destClass;
		// System.out.println("Destination classname: " + destinationClass.getName());
		switch (destinationClass.getName()) {

			case "java.lang.String": {
				if (value == null) {
					stringValue = "null";
				} else {
					stringValue = (String) value;
				}
				break;
			}

			case "int":
			case "java.lang.Integer": {
				if (value == null) {
					stringValue = "null";
				} else {
					stringValue = String.valueOf((int) value);
				}
				break;
			}

			case "double":
			case "java.lang.Double": {
				if (value == null) {
					stringValue = "null";
				} else {
					stringValue = String.valueOf((double) value);
				}
				break;
			}

			case "float":
			case "java.lang.Float": {
				if (value == null) {
					stringValue = "null";
				} else {
					stringValue = String.valueOf((float) value);
				}
				break;
			}

			case "long":
			case "java.lang.Long": {
				if (value == null) {
					stringValue = "null";
				} else {
					stringValue = String.valueOf((long) value);
				}
				break;
			}

			case "short":
			case "java.lang.Short": {
				if (value == null) {
					stringValue = "null";
				} else {
					stringValue = String.valueOf((short) value);
				}
				break;
			}

			case "char":
			case "java.lang.Character": {
				if (value == null) {
					stringValue = "null";
				} else {
					stringValue = String.valueOf((char) value);
				}
				break;
			}

			case "boolean":
			case "java.lang.Boolean": {
				if (value == null) {
					stringValue = "null";
				} else {
					stringValue = String.valueOf((boolean) value);
				}
				break;
			}

			default:
				break;
		}
		return stringValue;
	}

	public static Object convertFieldValueToObject(Class<?> destinationClass, String value) {
		Object objectValue = null;
		// System.out.println("Destination classname: " + destinationClass.getName());
		// System.out.println("Value: " + value);
		if (value.equalsIgnoreCase("null")) {
			return null;
		}
		switch (destinationClass.getName()) {

			case "java.lang.String": {
				objectValue = value;
				break;
			}

			case "int":
			case "java.lang.Integer": {
				objectValue = Integer.valueOf(value);
				break;
			}

			case "double":
			case "java.lang.Double": {
				objectValue = Double.valueOf(value);
				break;
			}

			case "float":
			case "java.lang.Float": {
				objectValue = Float.valueOf(value);
				break;
			}

			case "long":
			case "java.lang.Long": {
				objectValue = Long.valueOf(value);
				break;
			}

			case "short":
			case "java.lang.Short": {
				objectValue = Short.valueOf(value);
				break;
			}

			case "char":
			case "java.lang.Character": {
				objectValue = Character.valueOf(value.charAt(0));
				break;
			}

			case "boolean":
			case "java.lang.Boolean": {
				objectValue = Boolean.valueOf(value);
				break;
			}

			default:
				break;
		}
		return objectValue;
	}

	static boolean isList(Class<?> destinationClass) {
		return destinationClass.getName().startsWith("[L") || destinationClass.getName().startsWith("[I") || destinationClass.getName().startsWith("[C") || destinationClass.getName().startsWith("[F") || destinationClass.getName().startsWith("[D") || destinationClass.getName().startsWith("[J") || destinationClass.getName().startsWith("[S");
	}

}
