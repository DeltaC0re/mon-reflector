package net.ccmob.mon;

import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class MONWriter {

	private Object object = null;

	public MONWriter(Object object) {
		this.setObject(object);
	}

	/**
	 * @return the object
	 */
	private Object getObject() {
		return this.object;
	}

	/**
	 * @param object
	 *          the object to set
	 */
	private void setObject(Object object) {
		this.object = object;
	}

	public void serialize(PrintWriter writer) {
		writer.println("Root {");
		this.writeFields(this.getObject(), writer, 0);
		writer.println("}");
		writer.flush();
		writer.close();
	}

	private void writeFields(Object obj, PrintWriter writer, int tabIndent) {
		Class<?> coreClass;
		Object object = obj;
		// System.out.println("###############");
		// System.out.println("Object: ");
		// System.out.println(object);
		coreClass = object.getClass();

		// System.out.println("Class: " + coreClass.getName());
		// System.out.println("Class: " + coreClass.getSimpleName());
		// System.out.println("Class: " + coreClass.getCanonicalName());

		Field[] coreClassFields = coreClass.getDeclaredFields();

		for (Field objectField : coreClassFields) {
			if (Modifier.isFinal(objectField.getModifiers()) || Modifier.isStatic(objectField.getModifiers())) {
				// System.out.println("No need to write final fields.");
				continue;
			}
			objectField.setAccessible(true);
			String fieldName = objectField.getName();
			Class<?> fieldType = objectField.getType();
			// System.out.println("Field name: " + fieldName);
			try {
				Object fieldValue = objectField.get(object);
				// System.out.println("Field type: " + fieldValue.getClass().getName());
				String value = null;
				// System.out.println("Field value: ");
				// System.out.println(fieldValue);
				value = MONConverter.convertClassValueToString(fieldType, fieldValue);
				if (value == null) {
					if (MONConverter.isList(fieldType)) {
						switch (fieldType.getName().substring(0, 2)) {
							case "[I": {
								int[] valueList = (int[]) fieldValue;
								this.writeIntValueList(writer, valueList, tabIndent, fieldName);
								break;
							}

							case "[C": {
								char[] valueList = (char[]) fieldValue;
								this.writeCharValueList(writer, valueList, tabIndent, fieldName);
								break;
							}

							case "[F": {
								float[] valueList = (float[]) fieldValue;
								this.writeFloatValueList(writer, valueList, tabIndent, fieldName);
								break;
							}
							case "[D": {
								double[] valueList = (double[]) fieldValue;
								this.writeDoubleValueList(writer, valueList, tabIndent, fieldName);
								break;
							}
							case "[J": {
								long[] valueList = (long[]) fieldValue;
								this.writeLongValueList(writer, valueList, tabIndent, fieldName);
								break;
							}
							case "[S": {
								short[] valueList = (short[]) fieldValue;
								this.writeShortValueList(writer, valueList, tabIndent, fieldName);
								break;
							}
							case "[L": {
								Object[] valueList = (Object[]) fieldValue;
								this.writeObjectValueList(writer, valueList, tabIndent, fieldName);
								break;
							}
						}
					} else {
						writer.println(this.formTabs(tabIndent + 1) + fieldName + ":" + fieldValue.getClass().getName() + " {");
						this.writeFields(fieldValue, writer, tabIndent + 1);
						writer.println(this.formTabs(tabIndent + 1) + "}");
					}
				} else {
					writer.println(this.formTabs(tabIndent + 1) + fieldName + ": " + value);
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
			objectField.setAccessible(false);
		}
		// System.out.println("###############");
	}

	private void writeObjectValueList(PrintWriter writer, Object[] valueList, int tabIndent, String fieldName) {
		if (valueList.length == 0) {
			writer.println(this.formTabs(tabIndent + 1) + fieldName + ": []");
		} else {
			writer.println(this.formTabs(tabIndent + 1) + fieldName + ": [");
			for (Object listObject : valueList) {
				if (listObject == null) {
					writer.println(this.formTabs(tabIndent + 2) + "null");
					continue;
				}
				// System.out.println("Parsing object: " + listObject);
				String listObjectValue = MONConverter.convertClassValueToString(listObject.getClass(), listObject);
				if (listObjectValue == null) {
					writer.println(this.formTabs(tabIndent + 2) + listObject.getClass().getName() + " {");
					this.writeFields(listObject, writer, tabIndent + 2);
					writer.println(this.formTabs(tabIndent + 2) + "}");
					// writer.println(this.formTabs(tabIndent + 2) + "null");
				} else {
					writer.println(this.formTabs(tabIndent + 2) + listObjectValue);
				}
			}
			writer.println(this.formTabs(tabIndent + 1) + "]");
		}
	}

	private void writeIntValueList(PrintWriter writer, int[] valueList, int tabIndent, String fieldName) {
		if (valueList.length == 0) {
			writer.println(this.formTabs(tabIndent + 1) + fieldName + ": []");
		} else {
			writer.println(this.formTabs(tabIndent + 1) + fieldName + ": [");
			for (int listObject : valueList) {
				writer.println(this.formTabs(tabIndent + 2) + String.valueOf(listObject));
			}
			writer.println(this.formTabs(tabIndent + 1) + "]");
		}
	}

	private void writeShortValueList(PrintWriter writer, short[] valueList, int tabIndent, String fieldName) {
		if (valueList.length == 0) {
			writer.println(this.formTabs(tabIndent + 1) + fieldName + ": []");
		} else {
			writer.println(this.formTabs(tabIndent + 1) + fieldName + ": [");
			for (short listObject : valueList) {
				writer.println(this.formTabs(tabIndent + 2) + String.valueOf(listObject));
			}
			writer.println(this.formTabs(tabIndent + 1) + "]");
		}
	}

	private void writeCharValueList(PrintWriter writer, char[] valueList, int tabIndent, String fieldName) {
		if (valueList.length == 0) {
			writer.println(this.formTabs(tabIndent + 1) + fieldName + ": []");
		} else {
			writer.println(this.formTabs(tabIndent + 1) + fieldName + ": [");
			for (char listObject : valueList) {
				writer.println(this.formTabs(tabIndent + 2) + String.valueOf(listObject));
			}
			writer.println(this.formTabs(tabIndent + 1) + "]");
		}
	}

	private void writeFloatValueList(PrintWriter writer, float[] valueList, int tabIndent, String fieldName) {
		if (valueList.length == 0) {
			writer.println(this.formTabs(tabIndent + 1) + fieldName + ": []");
		} else {
			writer.println(this.formTabs(tabIndent + 1) + fieldName + ": [");
			for (float listObject : valueList) {
				writer.println(this.formTabs(tabIndent + 2) + String.valueOf(listObject));
			}
			writer.println(this.formTabs(tabIndent + 1) + "]");
		}
	}

	private void writeDoubleValueList(PrintWriter writer, double[] valueList, int tabIndent, String fieldName) {
		if (valueList.length == 0) {
			writer.println(this.formTabs(tabIndent + 1) + fieldName + ": []");
		} else {
			writer.println(this.formTabs(tabIndent + 1) + fieldName + ": [");
			for (double listObject : valueList) {
				writer.println(this.formTabs(tabIndent + 2) + String.valueOf(listObject));
			}
			writer.println(this.formTabs(tabIndent + 1) + "]");
		}
	}

	private void writeLongValueList(PrintWriter writer, long[] valueList, int tabIndent, String fieldName) {
		if (valueList.length == 0) {
			writer.println(this.formTabs(tabIndent + 1) + fieldName + ": []");
		} else {
			writer.println(this.formTabs(tabIndent + 1) + fieldName + ": [");
			for (long listObject : valueList) {
				writer.println(this.formTabs(tabIndent + 2) + String.valueOf(listObject));
			}
			writer.println(this.formTabs(tabIndent + 1) + "]");
		}
	}

	private String formTabs(int depth) {
		String tab = "";
		for (int i = 0; i < depth; i++) {
			tab += "\t";
		}
		return tab;
	}

}
