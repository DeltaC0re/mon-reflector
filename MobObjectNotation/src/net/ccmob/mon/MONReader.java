package net.ccmob.mon;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

public class MONReader {

	private Class<?>				reflector	= null;
	private BufferedReader	reader		= null;

	public MONReader(BufferedReader reader, Class<?> reflector) {
		this.setReader(reader);
		this.setReflector(reflector);
	}

	public Object deserialize() {
		if (this.getReader() == null || this.getReflector() == null) {
			System.err.println("Provided objects are null.");
			return null;
		}
		try {
			String line = this.reader.readLine();
			if (line.equalsIgnoreCase("Root {")) {
				Object endObject = this.parseObject(this.reader, this.reflector);
				this.reader.close();
				return endObject;
			} else {
				throw new Exception("Invalid mon file format");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private Field getFieldByName(Field[] list, String name) {
		for (Field f : list) {
			if (f.getName().equalsIgnoreCase(name)) {
				return f;
			}
		}
		return null;
	}

	private Object parseObject(BufferedReader reader, Class<?> coreClass) {

		// String className = coreClass.getName();
		// System.out.println("Coreclassname: " + className);
		try {
			Object instance = coreClass.newInstance();
			Field[] instanceFields = instance.getClass().getDeclaredFields();
			// for (Field f : instanceFields) {
			// System.out.println(" -> " + f.getName() + " - " + f.getModifiers() + " - " + f.getType().getName());
			// }
			String line = "";
			try {
				while ((line = reader.readLine()) != null) {
					line = line.trim();
					line = line.replaceAll("^\\t+", "");
					if (line.endsWith("{")) {
						String fieldNameTemp = line.substring(0, line.indexOf("{") - 1).trim();
						String fieldName = fieldNameTemp.split(":")[0];
						String fieldType = fieldNameTemp.split(":")[1];
						Field instanceField = this.getFieldByName(instanceFields, fieldName);
						if (instanceField != null) {
							if (Modifier.isFinal(instanceField.getModifiers()) || Modifier.isStatic(instanceField.getModifiers())) {
								// System.out.println("No need to write final fields.");
								continue;
							}
							instanceField.setAccessible(true);
							try {
								instanceField.set(instance, this.parseObject(reader, Class.forName(fieldType)));
							} catch (ClassNotFoundException e) {
								e.printStackTrace();
							}
							instanceField.setAccessible(false);
						}
					} else if (line.endsWith("[]")) {
						String fieldName = line.substring(0, line.indexOf("[") - 2).trim();
						Field instanceField = this.getFieldByName(instanceFields, fieldName);
						if (instanceField != null) {
							if (Modifier.isFinal(instanceField.getModifiers()) || Modifier.isStatic(instanceField.getModifiers())) {
								// System.out.println("No need to write final fields.");
								continue;
							}
							instanceField.setAccessible(true);
							if (instanceField != null) {
								switch (instanceField.getType().getName().substring(0, 2)) {
									case "[L": {
										// String listClassName = instanceField.getType().getName().substring(2);
										// System.out.println("ListClassName: " + listClassName);
										if (instanceField.getType().getName().endsWith("String")) {
											instanceField.set(instance, new String[0]);
										} else {
											instanceField.set(instance, new Object[0]);
										}
										break;
									}
									case "[I": {
										instanceField.set(instance, new int[0]);
										break;
									}
									case "[C": {
										instanceField.set(instance, new char[0]);
										break;
									}
									case "[F": {
										instanceField.set(instance, new float[0]);
										break;
									}
									case "[D": {
										instanceField.set(instance, new double[0]);
										break;
									}
									case "[J": {
										instanceField.set(instance, new long[0]);
										break;
									}
									case "[S": {
										instanceField.set(instance, new short[0]);
										break;
									}
								}
								instanceField.setAccessible(false);
							}
						}
					} else if (line.endsWith("[")) {
						String fieldName = line.substring(0, line.indexOf("[") - 2).trim();
						Field instanceField = this.getFieldByName(instanceFields, fieldName);
						if (Modifier.isFinal(instanceField.getModifiers()) || Modifier.isStatic(instanceField.getModifiers())) {
							// System.out.println("No need to write final fields.");
							continue;
						}
						if (instanceField != null) {
							instanceField.setAccessible(true);
							// String listClassName = instanceField.getType().getName().substring(2);
							// System.out.println("ListClassName: " + listClassName);
							switch (instanceField.getType().getName()) {
								case "[Ljava.lang.Object;": {
									ArrayList<Object> objects = new ArrayList<>();
									while (true) {
										String nextLine = reader.readLine();
										nextLine = nextLine.trim();
										nextLine = nextLine.replaceAll("^\\t+", "");
										if (nextLine.endsWith("{")) {
											String objectClassName = nextLine.substring(0, nextLine.indexOf("{") - 1);
											// System.out.println("ClassName of list object: " + objectClassName);
											try {
												objects.add(this.parseObject(reader, Class.forName(objectClassName)));
											} catch (ClassNotFoundException e) {
												e.printStackTrace();
											}
										} else if (nextLine.endsWith("null")) {
											objects.add(null);
										} else if (nextLine.endsWith("]")) {
											break;
										}
									}
									Object[] objectsList = new Object[objects.size()];
									int i = 0;
									for (Object o : objects) {
										objectsList[i] = o;
										i++;
									}
									instanceField.set(instance, objectsList);
									break;
								}

								case "[Ljava.lang.String;": {
									instanceField.set(instance, this.readStringArray(reader));
									break;
								}

								case "[Ljava.lang.Integer;": {
									instanceField.set(instance, this.readIntArray2(reader));
									break;
								}
								case "[I": {
									instanceField.set(instance, this.readIntArray(reader));
									break;
								}

								case "[Ljava.lang.Short;": {
									instanceField.set(instance, this.readShortArray2(reader));
									break;
								}
								case "[S": {
									instanceField.set(instance, this.readShortArray(reader));
									break;
								}

								case "[Ljava.lang.Float;": {
									instanceField.set(instance, this.readFloatArray2(reader));
									break;
								}
								case "[F": {
									instanceField.set(instance, this.readFloatArray(reader));
									break;
								}

								case "[Ljava.lang.Double;": {
									instanceField.set(instance, this.readDoubleArray2(reader));
									break;
								}
								case "[D": {
									instanceField.set(instance, this.readDoubleArray(reader));
									break;
								}

								case "[Ljava.lang.Long;": {
									instanceField.set(instance, this.readLongArray2(reader));
									break;
								}
								case "[J": {
									instanceField.set(instance, this.readLongArray(reader));
									break;
								}

								case "[Ljava.lang.Character;": {
									instanceField.set(instance, this.readCharArray2(reader));
									break;
								}
								case "[C": {
									instanceField.set(instance, this.readCharArray(reader));
									break;
								}

								case "[Ljava.lang.Boolean;": {
									instanceField.set(instance, this.readBooleanArray2(reader));
									break;
								}
								case "[B": {
									instanceField.set(instance, this.readBooleanArray(reader));
									break;
								}

								default:
									break;
							}
							instanceField.setAccessible(false);
						}
					} else if (line.indexOf(":") != -1) {
						String fieldName = line.substring(0, line.indexOf(":"));
						String fieldValue = "";
						if (line.length() != fieldName.length() + 1) {
							if (line.charAt(line.indexOf(":") + 1) != ' ') {
								fieldValue = line.substring(line.indexOf(":") + 1);
							} else {
								fieldValue = line.substring(line.indexOf(":") + 2);
							}
						}
						Field instanceField = this.getFieldByName(instanceFields, fieldName);
						if (instanceField != null) {
							Object instanceFieldValue = MONConverter.convertFieldValueToObject(instanceField.getType(), fieldValue);
							// System.out.println(instanceFieldValue);
							if (!(Modifier.isFinal(instanceField.getModifiers()) || Modifier.isStatic(instanceField.getModifiers()))) {
								instanceField.setAccessible(true);
								instanceField.set(instance, instanceFieldValue);
								instanceField.setAccessible(false);
							}
						}
					} else if (line.endsWith("}")) {
						return instance;
					}
				}
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return instance;
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * @return the reflector
	 */
	private Class<?> getReflector() {
		return this.reflector;
	}

	/**
	 * @param reflector
	 *          the reflector to set
	 */
	private void setReflector(Class<?> reflector) {
		this.reflector = reflector;
	}

	/**
	 * @return the reader
	 */
	private BufferedReader getReader() {
		return this.reader;
	}

	/**
	 * @param reader
	 *          the reader to set
	 */
	private void setReader(BufferedReader reader) {
		this.reader = reader;
	}

	private ArrayList<String> readArrayLines(BufferedReader reader) {
		String line;

		ArrayList<String> lines = new ArrayList<>();
		try {
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				line = line.replaceAll("^\\t+", "");
				if (line.endsWith("]")) {
					break;
				}
				lines.add(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lines;
	}

	private int[] readIntArray(BufferedReader reader) {
		ArrayList<String> lines = this.readArrayLines(reader);
		int[] array = new int[lines.size()];
		int i = 0;
		for (String value : lines) {
			array[i] = Integer.valueOf(value);
			i++;
		}
		return array;
	}

	private short[] readShortArray(BufferedReader reader) {
		ArrayList<String> lines = this.readArrayLines(reader);
		short[] array = new short[lines.size()];
		int i = 0;
		for (String value : lines) {
			array[i] = Short.valueOf(value);
			i++;
		}
		return array;
	}

	private float[] readFloatArray(BufferedReader reader) {
		ArrayList<String> lines = this.readArrayLines(reader);
		float[] array = new float[lines.size()];
		int i = 0;
		for (String value : lines) {
			array[i] = Float.valueOf(value);
			i++;
		}
		return array;
	}

	private double[] readDoubleArray(BufferedReader reader) {
		ArrayList<String> lines = this.readArrayLines(reader);
		double[] array = new double[lines.size()];
		int i = 0;
		for (String value : lines) {
			array[i] = Double.valueOf(value);
			i++;
		}
		return array;
	}

	private long[] readLongArray(BufferedReader reader) {
		ArrayList<String> lines = this.readArrayLines(reader);
		long[] array = new long[lines.size()];
		int i = 0;
		for (String value : lines) {
			array[i] = Long.valueOf(value);
			i++;
		}
		return array;
	}

	private char[] readCharArray(BufferedReader reader) {
		ArrayList<String> lines = this.readArrayLines(reader);
		char[] array = new char[lines.size()];
		int i = 0;
		for (String value : lines) {
			array[i] = value.charAt(0);
			i++;
		}
		return array;
	}

	private boolean[] readBooleanArray(BufferedReader reader) {
		ArrayList<String> lines = this.readArrayLines(reader);
		boolean[] array = new boolean[lines.size()];
		int i = 0;
		for (String value : lines) {
			array[i] = Boolean.valueOf(value);
			i++;
		}
		return array;
	}

	private Integer[] readIntArray2(BufferedReader reader) {
		ArrayList<String> lines = this.readArrayLines(reader);
		Integer[] array = new Integer[lines.size()];
		int i = 0;
		for (String value : lines) {
			array[i] = Integer.valueOf(value);
			i++;
		}
		return array;
	}

	private Short[] readShortArray2(BufferedReader reader) {
		ArrayList<String> lines = this.readArrayLines(reader);
		Short[] array = new Short[lines.size()];
		int i = 0;
		for (String value : lines) {
			array[i] = Short.valueOf(value);
			i++;
		}
		return array;
	}

	private Float[] readFloatArray2(BufferedReader reader) {
		ArrayList<String> lines = this.readArrayLines(reader);
		Float[] array = new Float[lines.size()];
		int i = 0;
		for (String value : lines) {
			array[i] = Float.valueOf(value);
			i++;
		}
		return array;
	}

	private Double[] readDoubleArray2(BufferedReader reader) {
		ArrayList<String> lines = this.readArrayLines(reader);
		Double[] array = new Double[lines.size()];
		int i = 0;
		for (String value : lines) {
			array[i] = Double.valueOf(value);
			i++;
		}
		return array;
	}

	private Long[] readLongArray2(BufferedReader reader) {
		ArrayList<String> lines = this.readArrayLines(reader);
		Long[] array = new Long[lines.size()];
		int i = 0;
		for (String value : lines) {
			array[i] = Long.valueOf(value);
			i++;
		}
		return array;
	}

	private Character[] readCharArray2(BufferedReader reader) {
		ArrayList<String> lines = this.readArrayLines(reader);
		Character[] array = new Character[lines.size()];
		int i = 0;
		for (String value : lines) {
			array[i] = value.charAt(0);
			i++;
		}
		return array;
	}

	private Boolean[] readBooleanArray2(BufferedReader reader) {
		ArrayList<String> lines = this.readArrayLines(reader);
		Boolean[] array = new Boolean[lines.size()];
		int i = 0;
		for (String value : lines) {
			array[i] = Boolean.valueOf(value);
			i++;
		}
		return array;
	}

	private String[] readStringArray(BufferedReader reader) {
		ArrayList<String> lines = this.readArrayLines(reader);
		String[] array = new String[lines.size()];
		int i = 0;
		for (String value : lines) {
			array[i] = value;
			i++;
		}
		return array;
	}

}
